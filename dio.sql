CREATE TABLE pessoas (
    id INT NOT NULL PRIMARY KEY AUTOINCREMENT,
    nome VARCHAR(30) NOT NULL,
    nascimento DATE
)

INSERT INTO pessoas (nome, nascimento) VALUES ('Luiz Eduardo', '2002-10-12')
INSERT INTO pessoas (nome, nascimento) VALUES ('Nicácio Rodrigues', '1996-07-17')
INSERT INTO pessoas (nome, nascimento) VALUES ('Lucas Kraus', '2002-06-26')
